/*
    MQTTBaseNode.cpp - Base MQTT node definition.
*/

#include "MQTTBaseNode.h"


// -------------------------------------------------------------------------
// MQTTNode
// -------------------------------------------------------------------------

MQTTNode::MQTTNode(const char* id)
: _id(id)
{

}

MQTTNode::MQTTNode(const char* id, const char* mqtt_in_topic, const char* mqtt_out_topic)
: _id(id), _mqtt_in_topic(mqtt_in_topic), _mqtt_out_topic(mqtt_out_topic)
{

}

MQTTNode::~MQTTNode()
{
    this->_mqtt_client->disconnect();
}

void MQTTNode::begin(MQTTClient& c, HardwareSerial& serial)
{
    // Set MQTT client
    this->_mqtt_client = &c;
    this->_mqtt_client->onMessageAdvanced(std::bind(&MQTTNode::process, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));

    // Set serial stream
    this->_serial = &serial;
}

bool MQTTNode::connected()
{
    return this->_mqtt_client->connected();
}

void MQTTNode::reconnect(int attempts, const char* username, const char* password)
{
    bool is_connected = false;
    for (int i = 0; i < attempts && !is_connected; i++) {
        is_connected = this->_mqtt_client->connect(this->_id, username, password);
        delay(200);
    }

    if (is_connected) {

        char msg[] = "Connection successful";
        this->_log_serial("MQTT_CONNECTION_STATE", msg, sizeof(msg), "log");

        // Subscribe to IN topic
        if (this->_mqtt_in_topic) {
            bool is_subscribed = this->_mqtt_client->subscribe(this->_mqtt_in_topic);
            if (is_subscribed) {
                char msg[] = "Subscribed to IN topic";
                this->_log_serial("MQTT_CONNECTION_STATE", msg, sizeof(msg), "log");

                // Publish INIT message
                if (this->_mqtt_out_topic) {
                    bool is_published = this->_mqtt_client->publish(this->_mqtt_out_topic, "INIT");
                    if (is_published) {
                        char msg[] = "INIT message published successfully";
                        this->_log_serial("MQTT_CONNECTION_STATE", msg, sizeof(msg), "log");
                    } else {
                        char msg[] = "INIT message failed";
                        this->_log_serial("MQTT_CONNECTION_STATE", msg, sizeof(msg), "log");
                    }
                }
            }
        }

    } else {

        char msg[] = "Connection failed";
        this->_log_serial("MQTT_CONNECTION_STATE", msg, sizeof(msg), "log");

    }
    
}

void MQTTNode::process(MQTTClient* client, char* topic, char* bytes, int length)
{
    this->_log_serial(topic, bytes, length);
}

void MQTTNode::loop()
{
    this->_mqtt_client->loop();
}

void MQTTNode::_log_serial(char* topic, char* payload, int length, const char* prefix)
{
    // Timestamp
    this->_serial->print("(");
    this->_serial->print(millis());
    this->_serial->print(")");
    // ID
    this->_serial->print("[id:");
    this->_serial->print(this->_id);
    this->_serial->print("]");
    // Topic
    this->_serial->print("[");
    this->_serial->print(prefix);
    this->_serial->print(":");
    this->_serial->print(topic);
    this->_serial->print("] ");
    // Payload
    for (int i=0; i<length; i++) {
        this->_serial->print(payload[i]);
    }
    this->_serial->println();
}

// -------------------------------------------------------------------------
// MQTTNodeContainer
// -------------------------------------------------------------------------

MQTTNodeContainer::MQTTNodeContainer(int count, ...)
{
    va_list ptr;           // Declare pointer to the argument list
    va_start(ptr, count);  // Initializing argument to the list pointer

    for (int i = 0; i < count; i++) {
        this->add(*va_arg(ptr, MQTTNode*));
    }

    va_end(ptr);  // Ending argument list traversal
}

void MQTTNodeContainer::add(MQTTNode& node)
{
    this->_nodes.push_back(&node);
}

bool MQTTNodeContainer::connected()
{
    bool is_connected;
    for (auto it = this->_nodes.begin(); it != this->_nodes.end(); ++it) {
        is_connected = (*it)->connected();
        if (!is_connected) break;
    }
    return is_connected;
}

void MQTTNodeContainer::reconnect(int attempts, const char* username, const char* password)
{
    for (auto it = this->_nodes.begin(); it != this->_nodes.end(); ++it) {
        if (!(*it)->connected()) {
            (*it)->reconnect(attempts, username, password);
        }
    }
}

void MQTTNodeContainer::loop()
{
    for (auto it = this->_nodes.begin(); it != this->_nodes.end(); ++it) {
        (*it)->loop();
    }
}