# SEN0137 DHT22 Temperature & Humidity Sensor
    https://www.dfrobot.com/product-1102.html
    https://www.dfrobot.com/blog-910.html

# SEN0127 MQ2 Analog Gas Sensor
    https://www.dfrobot.com/product-681.html
    https://wiki.dfrobot.com/Analog_Gas_Sensor_SKU_SEN0127

# SEN0132 MQ7 Analog Carbon Monoxide Sensor
    https://www.dfrobot.com/product-686.html
    https://wiki.dfrobot.com/Carbon_Monoxide_Gas_Sensor_MQ7___SKU_SEN0132_

# SEN0198 Analog High Temperature Sensor
    https://www.dfrobot.com/product-1474.html
    https://wiki.dfrobot.com/HighTemperatureSensor_SKU_SEN0198

# SEN0171 Digital PIR (Motion) Sensor
    https://www.dfrobot.com/product-1140.html
    https://wiki.dfrobot.com/PIR_Motion_Sensor_V1.0_SKU_SEN0171

# GL-MT300N-V2 300M Mini Smart Router
    https://store.gl-inet.com/collections/mini-smart-router/products/gl-mt300n-v2-mini-smart-router
    https://docs.gl-inet.com/en/3/setup/mini_router/first_time_setup/

# ESP32 AZ-Delivery Dev Kit
    https://www.az-delivery.de/products/esp-32-dev-kit-c-v4
