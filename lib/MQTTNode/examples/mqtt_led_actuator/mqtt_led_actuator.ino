/*
  MQTT LED actuator example.
  
  Uses MQTT client to subscribe to IN TOPIC and perform custom LED operations. Available commands:
    * on
    * off
    * blink <N_CYCLES> <PERIOD_MS> <DUTY_CYCLE>
*/

#include <WiFi.h>
#include <MQTT.h>
#include <MQTTNode.h>

/* Network connection settings */
const char* ssid              = "GL-MT300N-V2-5fa";
const char* password          = "goodlife";

/* MQTT settings */
const char* mqtt_server       = "192.168.8.1";
const int mqtt_port           = 1883;

// WiFi clients
WiFiClient espWiFiClient;

// MQTT clients
MQTTClient client;

// MQTT LED actuator
LEDActuator LED("id0001", 2, "topic/in", "topic/out");


void setup()
{
  // Initialize serial communication
  Serial.begin(115200);

  // Connecting to the WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  Serial.println();

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print('.');
  }

  Serial.println();
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  // MQTT client connect
  client.begin(mqtt_server, mqtt_port, espWiFiClient);

  // MQTT node setup
  LED.begin(client);
}

void loop()
{
  // Check connection to MQTT server
  if (!LED.connected()) {
    // Reconnect and subscribe in topic
    LED.reconnect();
  }

  LED.loop();  // Process in calls
}