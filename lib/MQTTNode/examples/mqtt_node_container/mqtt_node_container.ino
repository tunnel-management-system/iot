/*
  MQTT node container operation example.
  
  Uses MQTT node container to manage multiple node objects.
*/

#include <WiFi.h>
#include <MQTT.h>
#include <MQTTNode.h>

/* Network connection settings */
const char* ssid              = "GL-MT300N-V2-5fa";
const char* password          = "goodlife";

/* MQTT settings */
const char* mqtt_server       = "192.168.8.1";
const int mqtt_port           = 1883;

// WiFi clients
WiFiClient espWiFiClient1, espWiFiClient2, espWiFiClient3;

// MQTT clients
MQTTClient client1, client2, client3;

// MQTT nodes
MQTTNode node1("id0001", "topic1/in", "topic1/out");
MQTTNode node2("id0002", "topic2/in", "topic2/out");
MQTTNode node3("id0003", "topic3/in", "topic3/out");

// MQTT node container
MQTTNodeContainer container(3, node1, node2, node3);


void setup()
{
  // Initialize serial communication
  Serial.begin(115200);

  // Connecting to the WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  Serial.println();

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print('.');
  }

  Serial.println();
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  // MQTT clients connect
  client1.begin(mqtt_server, mqtt_port, espWiFiClient1);
  client2.begin(mqtt_server, mqtt_port, espWiFiClient2);
  client3.begin(mqtt_server, mqtt_port, espWiFiClient3);

  // MQTT nodes setup
  node1.begin(client1);
  node2.begin(client2);
  node3.begin(client3);
}

void loop()
{
  // Check connection to MQTT server
  if (!container.connected()) {
    // Reconnect and subscribe in topic
    container.reconnect();
  }

  container.loop();  // Process in calls
}