/*
  MQ2 Analog Gas Sensor test.
*/

void setup()
{
  Serial.begin(115200);
}

void loop()
{
  Serial.print("Gas: ");
  Serial.println(analogRead(4));
  delay(200);
}
