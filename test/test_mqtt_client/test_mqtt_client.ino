/*
  MQTT client test with DHT22 Temperature & Humidity sensor for ESP32 device
*/

#include <WiFi.h>
#include <PubSubClient.h>
#include <DHTesp.h>

/* Network connection settings */
const char* ssid     = "GL-MT300N-V2-5fa";
const char* password = "goodlife";

/* MQTT settings */
const char* mqtt_server = "192.168.8.1";
const char* mqtt_dev_id = "ID001";

/* DHT22 sensor settings */
uint8_t dht22_pin = 26;


WiFiClient espWiFiClient;
PubSubClient MQTTclient(espWiFiClient);

DHTesp dht;


void reconnect() {
  // Loop until we're reconnected
  while (!MQTTclient.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (MQTTclient.connect(mqtt_dev_id)) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(MQTTclient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}


void setup()
{
  // Initialize serial communication
  Serial.begin(115200);

  // Setup DHT22
  dht.setup(dht22_pin, DHTesp::DHT22);

  // Connecting to the WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  Serial.println();

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print('.');
  }

  Serial.println();
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  // Set MQTT client
  MQTTclient.setServer(mqtt_server, 1883);
  MQTTclient.setCallback(callback);
}

void loop()
{
  // Connect to MQTT broker
  if (!MQTTclient.connected()) {
    reconnect();
  }

  float temperature = dht.getTemperature();  // Read temperature
  float humidity    = dht.getHumidity();     // Read humidity

  char msg[50];
  sprintf(msg, "Temperature: %.2fºC, Humidity: %.2f%%", temperature, humidity);

  MQTTclient.publish("dht22/test", msg);  // Publish data
  MQTTclient.loop();                      // Wait for incoming messages

  delay(1000);
}
