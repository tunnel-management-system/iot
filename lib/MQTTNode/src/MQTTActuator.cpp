/*
    MQTTActuator.cpp - MQTT actuator definition based on MQTT node object.
*/

#include "MQTTActuator.h"


LEDActuator::LEDActuator(const char* id, int pin)
: MQTTNode(id), _pin(pin)
{
    // Set LED pin
    pinMode(this->_pin, OUTPUT);
    this->_off();
}

LEDActuator::LEDActuator(const char* id, const int pin, const char* mqtt_in_topic, const char* mqtt_out_topic)
: MQTTNode(id, mqtt_in_topic, mqtt_out_topic), _pin(pin)
{
    // Set LED pin
    pinMode(this->_pin, OUTPUT);
    this->_off();
}

LEDActuator::~LEDActuator()
{
    this->_off();
}

void LEDActuator::process(MQTTClient* client, char* topic, char* bytes, int length)
{
    MQTTNode::process(client, topic, bytes, length);

    char format[] = "%s %d %d %f";
    char command[10];

    int n_cycles, period_ms;
    float duty_cycle;

    if (int n = sscanf(bytes, format, command, &n_cycles, &period_ms, &duty_cycle)) {

        if (!strcmp(command, "on")) {
            this->_on();
        } else if (!strcmp(command, "off")) {
            this->_off();
        } else if (!strcmp(command, "blink")) {
            if (n == 4) this->_blink(n_cycles, period_ms, duty_cycle);
        }

    }
}

void LEDActuator::_on()
{
    digitalWrite(this->_pin, HIGH);
}

void LEDActuator::_off()
{
    digitalWrite(this->_pin, LOW);
}

void LEDActuator::_blink(int n_cycles, int period_ms, float duty_cicle)
{
    this->_off();

    for (int i = 0; i < n_cycles; i++) {
        this->_on();
        delay((int)(period_ms * duty_cicle));
        this->_off();
        delay((int)(period_ms * (1 - duty_cicle)));
    }
}
