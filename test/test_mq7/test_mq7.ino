/*
  MQ7 Carbon Monoxide Sensor test.
*/

void setup()
{
  Serial.begin(115200);
}

void loop()
{
  Serial.print("CO: ");
  Serial.println(analogRead(4));
  delay(200);
}
